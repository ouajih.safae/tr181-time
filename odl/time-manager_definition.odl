%define {
    /**
     * This object contains global parameters relating to the NTP or SNTP time clients and or servers that are active in the CPE.
     *
     * @version 1.0
     */
    %persistent object Time {
         /**
         * Enables or disables all the NTP or SNTP time clients and servers.
         *
         * @version 1.0
         */
        %persistent bool Enable {
            default true;
        }

        /**
         *Reflects the global time synchronisation status of the CPE. Enumeration of: \n
         *  - Disabled (Indicates that the CPE's time client services are disabled). \n
         *  - Unsynchronized (Indicates that the CPE's absolute time has not yet been set by any of the configured time clients). \n
         *  - Synchronized (Indicates that the CPE has acquired accurate absolute time; its current time is accurate.
         * One or more time client was able to configure the time of the CPE). \n
         *  - Error_FailedToSynchronize () \n
         *  - Error (MAY be used by the CPE to indicate a locally defined error condition. None of the configured Time clients were able to synchronize the time, OPTIONAL)
         *
         * @version 1.0
         */
        %read-only string Status {
            on action validate call check_enum ["Disabled", "Unsynchronized", "Synchronized", "Error_FailedToSynchronize","Error"];
            default "Unsynchronized";
        }

        /**
         * Request Status parameter update
         *
         * @version 1.0
         */
        string update_status();

        /**
         * Current date and time in CPE's local time zone.
         *
         * @version 1.0
         */
         %volatile %read-only datetime CurrentLocalTime {
            on action read call read_current_local_time;
         }

         /**
          * POSIX compliant string, e.g.Local time zone name and daylight saving time, encoded as an
          * EEE 1003.1 encoded string. e.g. “EST+5EDT,M4.1.0/2,M10.5.0/2”.
          *
          * @version 1.0
          */
         %persistent string LocalTimeZone {
              default "GMT0";
              on action validate call check_local_timezone;
         }

         /**
          * List of the supported time modules.
          *
          * @version 1.0
          */
         %persistent %protected csv_string SupportedTimeControllers = "mod-time-chrony-uci";
         /**
          * The time daemon module which is being used.
          *
          * @version 1.0
          */
         %persistent %protected string TimeController {
             default "mod-time-chrony-uci";
         }
         /**
          * The directory where the modules are located on target
          *
          * @version 1.0
          */
        %persistent %protected string mod-dir = "/usr/lib/amx/time-manager/modules";
    }
}

include "time-manager_client_definition.odl";
include "time-manager_server_definition.odl";

%populate {
    on event "dm:object-changed" call time_toggle
        filter 'path == "Time." && contains("parameters.Enable")';

    on event "dm:object-changed" call update_local_timezone
        filter 'path == "Time." && contains("parameters.LocalTimeZone")';

    on event "dm:object-changed" call count_changed
        filter 'path matches "Time." && contains("parameters.ClientNumberOfEntries")';

    on event "dm:object-changed" call count_changed
        filter 'path matches "Time." && contains("parameters.ServerNumberOfEntries")';

    //on event "*" call print_event;
}
