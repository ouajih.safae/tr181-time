%define {
    select Time {
        /**
         * This object contains parameters relating to an NTP or SNTP time client instance.
         * At most one entry in this table can exist with a given value for Alias.
         *
         * @version 1.0
        */
        %persistent object Client[] {
            on action destroy call dm_client_action_on_destroy;
            counted with ClientNumberOfEntries;
            /**
             * Enables or disables the NTP or SNTP time client.
             *
             * @version 1.0
            */
            %persistent bool Enable {
                default true;
           }
            /**
             * Status of Time support for this client. Enumeration of: \n
             *  - Disabled (Indicates that the time client service is being disabled) \n
             *  - Unsynchronized (Indicates that the time client absolute time has not yet been set) \n
             *  - Synchronized (Indicates that the time client has acquired accurate absolute time; its current time is accurate) \n
             *  - Error (MAY be used by the time client to indicate a locally defined error condition, OPTIONAL) \n
             *
             * @version 1.0
            */
            %read-only string Status {
                on action validate call check_enum ["Disabled", "Unsynchronized", "Synchronized", "Error_FailedToSynchronize","Error"];
                default "Error_FailedToSynchronize";
            }
            /**
             * [Alias] A non-volatile unique key used to reference this instance. Alias provides a mechanism for a Controller to label this instance for future reference. \n
             * The following mandatory constraints MUST be enforced: \n
             *  - The value MUST NOT be empty. \n
             *  - The value MUST start with a letter. \n
             *  - If the value is not assigned by the Controller at creation time, the Agent MUST assign a value with an "cpe-" prefix. \n
             *  - If the value isn't assigned by the Controller on creation, the Agent MUST choose an initial value that doesn't conflict with any existing entries. \n
             * This is a non-functional key and its value MUST NOT change once it's been assigned by the Controller or set internally by the Agent.
             *
             * @version 1.0
            */
            %unique %key string Alias;
            /**
             * Specifies in which mode the NTP client must be run. Enumeration of: \n
             *  - Unicast (Support for the NTP client in unicast mode) \n
             *  - Broadcast (Support for the NTP client in broadcast mode) \n
             *  - Multicast (Support for the NTP client in multicast mode) \n
             *  - Manycast (Support for the NTP client in manycast mode)
             *
             * @version 1.0
            */
            %persistent string Mode {
                on action validate call check_enum ["Unicast", "Broadcast", "Multicast", "Manycast"];
                default "Manycast";
            }
            /**
             * Specify the port used to send NTP packets.
             *
             * @version 1.0
            */
            %persistent uint32 Port {
                default 123;
            }
            /**
             * Specifies the supported NTP version. Possible versions are 1-4.
             *
             * @version 1.0
            */
            %persistent uint32 Version {
                default 4;
            }
            /**
             * Comma-separated list of strings. Points to a CSV list of NTP servers or pools. A NTP server can either be specified as an IP address or a host name. \n
             * It is expected that the NTP client resolves multiple addresses which can change over time when ResolveAddresses is enabled.
             *
             * @version 1.0
            */
            %persistent csv_string Servers {
                on action validate call check_maximum_length 256;
            }
            /**
             * When this option is enabled the NTP client must resolve the multiple addresses associated with the host name(s) that are specified in the Servers field.
             *
             * @version 1.0
            */
            %persistent bool ResolveAddresses {
                default false;
            }
            /**
             * When ResolveAddresses is enabled, This parameter specifies the maxium number of IP addresses that the NTP client can resolve.
             * 0 means that all addresses must be resolved.
             *
             * @version 1.0
            */
            %persistent uint32 ResolveMaxAddresses {
                default 6;
            }
            /**
             * Use symmetric active association mode. This device may provide synchronization to the configured NTP server.
             *
             * @version 1.0
            */
            %persistent bool Peer;
            /**
             * This is the minimum polling interval, in seconds to the power of two, allowed by any peer of the Internet system, currently set to 6 (64 seconds).
             *
             * @version 1.0
            */
            %persistent uint32 MinPoll {
                default 6;
            }
            /**
             * This is the maximum polling interval, in seconds to the power of two, allowed by any peer of the Internet system, currently set to 10 (1024 seconds).
             *
             * @version 1.0
            */
            %persistent uint32 MaxPoll {
                default 10;
            }
            /**
             * If the {{|IBurst}} parameter is enabled, and this is the first packet sent when the server has been unreachable, the client sends a burst.
             * This is useful to quickly reduce the synchronization distance below the distance threshold and synchronize the clock.
             *
             * @version 1.0
            */
            %persistent bool IBurst {
                default false;
            }
            /**
             * if the {{|Burst}} parameter is enabled and the server is reachable and a valid source of synchronization is available,
             * the client sends a burst of BCOUNT (8) packets at each poll interval. The interval between packets in the burst is two seconds.
             * This is useful to accurately measure jitter with long poll intervals.
             *
             * @version 1.0
            */
            %persistent uint32 Burst {
                default 8;
            }
            /**
             * The value MUST be the Path Name of a row in the IP.Interface table. If the referenced object is deleted, the parameter value MUST be set to an empty string.
             * The IP Interface associated with the Client entry.
             *
             * @version 1.0
            */
            %persistent string Interface {
                on action validate call check_maximum_length 256;
            }
            /**
             * Specifies how the client sockets are bound. Enumeration of: \n
             *  - Address (The client sockets are bound to a local IP address) \n
             *  - Device (The client sockets are bound to a network device. This can be useful when the local address is dynamic)
             *
             * @version 1.0
            */
            %persistent string BindType;
            /**
             * This object contains parameters relating to enabling security for the NTP client.
             *
             * @version 1.0
            */
            %persistent object Authentication {
                /**
                 * Enables or disables authentication of the NTP client.
                 *
                 * @version 1.0
                */
                %persistent bool Enable {
                    default false;
                }
                /**
                 * The value MUST be the Path Name of a row in the Security.Certificate table.
                 * If the referenced object is deleted, the parameter value MUST be set to an empty string. Points to the certificate that must be used by the NTS-KE client.
                 *
                 * @version 1.0
                */
                %persistent string Certificate;
                /**
                 * On this port the NTS Key Establishment protocol is being provided.
                 *
                 * @version 1.0
                */
                %persistent uint32 NTSPort {
                    default 4460;
                }
            }
            /**
             * This object specifies the statistic parameters for the NTP client.
             *
             * @version 1.0
            */
            %persistent object Stats {
                /**
                 * 	[StatsCounter32] Specifies the number of packets sent.
                 *
                 * @version 1.0
                */
                uint32 PacketsSent;
                /**
                 * [StatsCounter32] Specifies the number of failed sent packets.
                 *
                 * @version 1.0
                */
                uint32 PacketsSentFailed;
                /**
                 * [StatsCounter32] Specifies the number of received packets.
                 *
                 * @version 1.0
                */
                uint32 PacketsReceived;
                /**
                 * [StatsCounter32] Specifies the number of dropped packets.
                 *
                 * @version 1.0
                */
                uint32 PacketsDropped;
            }
        }
    }
}

%populate {
    on event "dm:object-changed" call dm_client_toggle
        filter 'path matches "Time\.Client\.[0-9]+\." && contains("parameters.Enable")';
    on event "dm:object-changed" call dm_client_interface_changed
        filter 'path matches "Time\.Client\.[0-9]+\." && contains("parameters.Interface")';
    on event "dm:object-changed" call dm_client_changed
        filter 'path matches "Time\.Client\.[0-9]+\." && !contains("parameters.Status")';
    on event "dm:instance-added" call dm_client_added
        filter 'object == "Time.Client."';
    //on event "*" call print_event;
}
