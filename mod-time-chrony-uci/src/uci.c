/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "uci.h"
#include "utils.h"
#include "stdlib.h"

#define ME "uci"
#define CFG_RELOAD_TIMEOUT (400)
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>

static bool uci_service_reload(const char* service, const char* command) {
    bool rc = false;
    amxp_subproc_t* reload_config = NULL;
    const char* args[] = {
        service,
        command,
        NULL
    };

    when_null(service, exit);
    when_null(command, exit);

    amxp_subproc_new(&reload_config);

    switch(amxp_subproc_vstart_wait(reload_config, CFG_RELOAD_TIMEOUT, (char**) args)) {
    case -1:
    {
        SAH_TRACEZ_ERROR(ME, "Reload config script execution error");
    }
    break;
    case 0: {
        rc = true;
    }
    break;
    case 1: {
        SAH_TRACEZ_ERROR(ME, "Reload config script stuck. Kill it");
        amxp_subproc_kill(reload_config, SIGKILL);
    }
    break;
    }
    amxp_subproc_delete(&reload_config);

exit:
    return rc;
}

static int uci_commit_time(amxb_bus_ctx_t* ctx) {
    int retval = 0;

    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "config", "system");
    amxc_var_add_key(cstring_t, &args, "section", "ntp");
    retval = amxb_call(ctx, "uci.", "commit", &args, &ret, 3);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "config", "chrony");
    retval += amxb_call(ctx, "uci.", "commit", &args, &ret, 3);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return retval;
}

static int uci_call(const char* method, amxc_var_t* args, amxc_var_t* ret) {
    int rv = 0;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("uci.");

    rv = amxb_call(ctx, "uci.", method, args, ret, 5);
    if(rv != AMXB_STATUS_OK) {
        SAH_TRACEZ_INFO(ME, "failed to call UCI %s, function returned %d", method, rv);
        goto exit;
    }

    rv = uci_commit_time(ctx);

exit:
    return rv;
}

static int uci_timezone_set(const char* timezone, amxc_var_t* ret) {
    bool retval = 0;
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);
    amxc_var_t* values = NULL;

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "system");
    amxc_var_add_key(cstring_t, &uci_args, "type", "system");

    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(cstring_t, values, "timezone", timezone);

    retval = uci_call("set", &uci_args, ret);
    amxc_var_clean(&uci_args);

    when_failed(retval, exit);
    retval = uci_service_reload("/etc/init.d/system", "reload");

exit:
    return retval;
}

static int uci_enable_set(bool server, bool enable, amxc_var_t* ret) {
    int retval = -1;
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);
    amxc_var_t* values = NULL;

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "system");
    amxc_var_add_key(cstring_t, &uci_args, "section", "ntp");
    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);

    if(server) {
        amxc_var_add_key(bool, values, "enable_server", enable);
    } else {
        amxc_var_add_key(bool, values, "enabled", enable);
    }

    retval = uci_call("set", &uci_args, ret);

    amxc_var_clean(&uci_args);
    return retval;


}

static int uci_servers_set(amxc_string_t* servers, amxc_var_t* ret) {
    int retval = -1;
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);
    amxc_var_t* values = NULL;

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "system");
    amxc_var_add_key(cstring_t, &uci_args, "section", "ntp");
    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(cstring_t, values, "server", amxc_string_get(servers, 0));

    retval = uci_call("set", &uci_args, ret);

    amxc_var_clean(&uci_args);
    return retval;

}

static bool get_server_enable(amxc_var_t* args) {
    bool enable = false;
    uint32_t servers = GETP_UINT32(args, "Time.servers");
    char* server_enable_path = NULL;
    uint32_t id = 0;
    when_null(args, exit);

    for(uint32_t i = 0; i < servers; i++) {
        id = i + 1;
        server_enable_path = get_server_parameter_path("enable", id);
        if(GETP_BOOL(args, server_enable_path)) {
            enable = true;
        }
        free(server_enable_path);
    }

exit:
    return enable;
}

static int align_with_tr181(amxc_var_t* args,
                            amxc_var_t* ret) {
    int retval = -1;
    when_null(args, exit);
    bool enable = GETP_BOOL(args, "Time.enable");
    bool client_enable = false;
    const char* timezone = GETP_CHAR(args, "Time.timezone");
    uint32_t clients = GETP_UINT32(args, "Time.clients");
    uint32_t id = 0;
    char* client_servers = NULL;
    char* client_enable_path = NULL;
    amxc_string_t servers;
    amxc_var_t* server;
    amxc_string_init(&servers, 0);
    amxc_var_new(&server);
    amxc_llist_t* servers_list = NULL;
    amxc_llist_new(&servers_list);

    amxc_string_appendf(&servers, " ");

    for(uint32_t i = 0; i < clients; i++) {
        id = i + 1;
        client_servers = get_client_parameter_path("servers", id);
        client_enable_path = get_client_parameter_path("enable", id);
        client_enable = GETP_BOOL(args, client_enable_path);
        if(client_enable) {
            amxc_llist_t* tmp = amxc_var_dyncast(amxc_llist_t, GETP_ARG(args, client_servers));
            amxc_llist_move(servers_list, tmp);
            amxc_llist_delete(&tmp, amxc_string_list_it_free);
        }
        free(client_servers);
        free(client_enable_path);
    }

    uci_enable_set(false, enable, ret);
    uci_enable_set(true, get_server_enable(args), ret);
    uci_timezone_set(timezone, ret);

    amxc_llist_for_each(iter, servers_list) {
        server = amxc_llist_it_get_data(iter, amxc_var_t, lit);
        amxc_string_appendf(&servers, "%s ", GETP_CHAR(server, NULL));
    }
    retval = uci_servers_set(&servers, ret);
    amxc_string_clean(&servers);
    amxc_var_delete(&server);
    amxc_llist_delete(&servers_list, variant_list_it_free);

exit:
    return retval;
}

static int delete_chrony_pools(amxc_var_t* ret) {
    int retval = 0;
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "chrony");
    amxc_var_add_key(cstring_t, &uci_args, "section", "@pool[0]");
    retval = uci_call("delete", &uci_args, ret);

    amxc_var_clean(&uci_args);

    return retval;
}

static void delete_chrony_dhcp_ntp_server(void) {
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);
    amxc_var_t* ret = NULL;

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "chrony");
    amxc_var_add_key(cstring_t, &uci_args, "section", "@dhcp_ntp_server[0]");

    uci_call("delete", &uci_args, ret);

    amxc_var_clean(&uci_args);
}

static void delete_chrony_makestep(void) {
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);
    amxc_var_t* ret = NULL;

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "chrony");
    amxc_var_add_key(cstring_t, &uci_args, "section", "@makestep[0]");

    uci_call("delete", &uci_args, ret);

    amxc_var_clean(&uci_args);
}

static int delete_chrony_allow(amxc_var_t* ret) {
    int retval = -1;
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "chrony");
    amxc_var_add_key(cstring_t, &uci_args, "section", "@allow[0]");
    retval = uci_call("delete", &uci_args, ret);

    amxc_var_clean(&uci_args);
    return retval;
}

static void create_chrony_dhcp_ntp_server(void) {
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);
    amxc_var_t* ret = NULL;

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "chrony");
    amxc_var_add_key(cstring_t, &uci_args, "type", "dhcp_ntp_server");
    uci_call("add", &uci_args, ret);

    amxc_var_clean(&uci_args);
}

static void create_chrony_makestep(void) {
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);
    amxc_var_t* ret = NULL;

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "chrony");
    amxc_var_add_key(cstring_t, &uci_args, "type", "makestep");
    uci_call("add", &uci_args, ret);

    amxc_var_clean(&uci_args);
}

static void create_chrony_pool(void) {
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);
    amxc_var_t* ret = NULL;

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "chrony");
    amxc_var_add_key(cstring_t, &uci_args, "type", "pool");
    uci_call("add", &uci_args, ret);

    amxc_var_clean(&uci_args);
}

static void create_chrony_allow(void) {
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);
    amxc_var_t* ret = NULL;

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "chrony");
    amxc_var_add_key(cstring_t, &uci_args, "type", "allow");
    uci_call("add", &uci_args, ret);

    amxc_var_clean(&uci_args);
}

static void configure_chrony_dhcp_ntp_server(void) {
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);
    amxc_var_t* values = NULL;
    amxc_var_t* ret = NULL;

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "chrony");
    amxc_var_add_key(cstring_t, &uci_args, "section", "@dhcp_ntp_server[0]");

    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(cstring_t, values, "iburst", "yes");
    uci_call("set", &uci_args, ret);

    amxc_var_clean(&uci_args);
}

static void configure_chrony_makestep(void) {
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);
    amxc_var_t* values = NULL;
    amxc_var_t* ret = NULL;


    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "chrony");
    amxc_var_add_key(cstring_t, &uci_args, "section", "@makestep[0]");

    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(cstring_t, values, "threshold", "1.0");
    amxc_var_add_key(uint32_t, values, "limit", 3);
    uci_call("set", &uci_args, ret);

    amxc_var_clean(&uci_args);
}

static int count_allowed(amxc_var_t* args) {
    int allowed = 0;
    uint32_t servers = GETP_UINT32(args, "Time.servers");
    bool server_enable = false;
    char* server_interface_path = NULL;
    char* server_enable_path = NULL;
    uint32_t id = 0;

    for(uint32_t i = 0; i < servers; i++) {
        id = i + 1;
        server_interface_path = get_server_parameter_path("interface", id);
        server_enable_path = get_server_parameter_path("enable", id);
        server_enable = GETP_BOOL(args, server_enable_path);
        if(server_enable) {
            allowed += 1;
        }
        free(server_interface_path);
        free(server_enable_path);
    }
    return allowed;
}

static int count_chrony_ntp_servers(amxc_var_t* args) {
    int servers = 0;
    uint32_t clients = GETP_UINT32(args, "Time.clients");
    bool client_enable = false;
    char* client_servers_path = NULL;
    char* client_enable_path = NULL;
    uint32_t id = 0;

    for(uint32_t i = 0; i < clients; i++) {
        id = i + 1;
        client_servers_path = get_client_parameter_path("servers", id);
        client_enable_path = get_client_parameter_path("enable", id);
        client_enable = GETP_BOOL(args, client_enable_path);
        if(client_enable) {
            servers += amxc_llist_size(amxc_var_constcast(amxc_llist_t, GETP_ARG(args, client_servers_path)));

        }
        free(client_servers_path);
        free(client_enable_path);
    }

    return servers;
}

static void configure_chrony_allow(amxc_var_t* args) {
    uint32_t servers = GETP_UINT32(args, "Time.servers");
    uint32_t id = 0;
    amxc_var_t uci_args;
    amxc_var_t* values = NULL;
    amxc_var_t* ret = NULL;
    char* server_interface_path = NULL;
    char* server_enable_path = NULL;
    bool server_enable = false;
    uint32_t number = 0;

    for(uint32_t i = 0; i < servers; i++) {
        id = i + 1;
        server_interface_path = get_server_parameter_path("interface", id);
        server_enable_path = get_server_parameter_path("enable", id);
        server_enable = GETP_BOOL(args, server_enable_path);

        if(server_enable) {
            amxc_string_t allow;
            amxc_var_t* tmp = NULL;
            amxc_var_init(&uci_args);
            amxc_string_init(&allow, 0);
            amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
            amxc_var_add_key(cstring_t, &uci_args, "config", "chrony");
            amxc_string_setf(&allow, "@allow[%d]", number);
            tmp = amxc_var_add_new_key(&uci_args, "section");
            amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&allow));
            values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
            amxc_var_add_key(cstring_t, values, "interface", GETP_CHAR(args, server_interface_path));

            uci_call("set", &uci_args, ret);
            number += 1;

            amxc_var_clean(&uci_args);
            amxc_string_clean(&allow);

        }
        free(server_interface_path);
        free(server_enable_path);
    }
}

static void configure_chrony_pool(amxc_var_t* args) {
    uint32_t clients = GETP_UINT32(args, "Time.clients");
    int number = 0;

    for(uint32_t i = 0; i < clients; i++) {
        char* client_servers_path = NULL;
        char* client_enable_path = NULL;
        char* client_iburst_path = NULL;
        char* client_maxpoll_path = NULL;
        bool client_enable = false;
        const char* iburst = NULL;
        uint32_t id = 0;
        uint32_t maxpoll = 0;

        id = i + 1;
        client_servers_path = get_client_parameter_path("servers", id);
        client_enable_path = get_client_parameter_path("enable", id);
        client_iburst_path = get_client_parameter_path("iburst", id);
        client_maxpoll_path = get_client_parameter_path("maxpoll", id);
        client_enable = GETP_BOOL(args, client_enable_path);
        iburst = (GETP_BOOL(args, client_iburst_path) ? "yes" : "no");
        maxpoll = GETP_UINT32(args, client_maxpoll_path);
        if(client_enable) {
            amxc_var_for_each(server, GETP_ARG(args, client_servers_path)) {
                amxc_string_t pool;
                amxc_var_t uci_args;
                amxc_var_t* values = NULL;
                amxc_var_t* tmp = NULL;
                amxc_var_init(&uci_args);
                amxc_string_init(&pool, 0);
                amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
                amxc_var_add_key(cstring_t, &uci_args, "config", "chrony");
                amxc_string_setf(&pool, "%s%d%s", "@pool[", number, "]");
                tmp = amxc_var_add_new_key(&uci_args, "section");
                amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&pool));

                values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
                amxc_var_add_key(cstring_t, values, "iburst", iburst);
                amxc_var_add_key(uint32_t, values, "maxpoll", maxpoll);
                amxc_var_add_key(cstring_t, values, "hostname", GETP_CHAR(server, NULL));

                uci_call("set", &uci_args, NULL);
                number += 1;

                amxc_var_clean(&uci_args);
                amxc_string_clean(&pool);
            }
        }
        free(client_iburst_path);
        free(client_servers_path);
        free(client_enable_path);
        free(client_maxpoll_path);
    }
}

static int uci_apply_chrony_config(void) {
    return uci_service_reload("/usr/bin/time-manager_uci", "chronyd_uci_reload") ? 0 : -1;
}

static void uci_write_chrony_config(amxc_var_t* args, amxc_var_t* ret) {
    int retval = 0;
    uint32_t try_removal = 0;
    uint32_t max_removal = 100;
    int count = 0;

    retval = delete_chrony_pools(ret);
    while(retval == 0 && max_removal > try_removal) {
        try_removal += 1;
        retval = delete_chrony_pools(ret);
    }
    try_removal = 0;
    retval = delete_chrony_allow(ret);
    while(retval == 0 && max_removal > try_removal) {
        try_removal += 1;
        retval = delete_chrony_allow(ret);
    }

    delete_chrony_dhcp_ntp_server();
    delete_chrony_makestep();

    create_chrony_dhcp_ntp_server();
    create_chrony_makestep();

    count = count_chrony_ntp_servers(args);
    for(int i = 0; i < count; i++) {
        create_chrony_pool();
    }
    count = count_allowed(args);
    for(int i = 0; i < count; i++) {
        create_chrony_allow();
    }

    configure_chrony_dhcp_ntp_server();
    configure_chrony_makestep();
    configure_chrony_pool(args);
    configure_chrony_allow(args);
}

int align_config(amxc_var_t* args, amxc_var_t* ret) {
    int retval = -1;
    when_failed_trace(align_with_tr181(args, ret), next, ERROR, "Failed to align config, this could mean a mismatch between some parameters");
next:
    uci_write_chrony_config(args, ret);
    when_failed_trace(uci_apply_chrony_config(), exit, ERROR, "Failed to write chrony config to file");
    retval = 0;

exit:
    return retval;
}
