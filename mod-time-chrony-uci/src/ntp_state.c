/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "mod_time_chrony.h"
#include "socket.h"
#include "control_msg.h"

#define LEAP_INDICATOR_UNSYNC (0x3u)

ntp_sync_state_t get_sync_state(void) {
    ntp_sync_state_t state = Error;

    int sock = chrony_def_socket();
    chrony_trac_req_t request;
    chrony_trac_rep_t resp;
    int n = 0;

    when_false(-1 != sock, exit);

    memset(&request, 0, sizeof(chrony_trac_req_t));
    memset(&resp, 0, sizeof(chrony_trac_rep_t));

    request.pkt_type = CMD_REQUEST;
    request.command = htons(REQ_TRACKING);
    request.res1 = 0;
    request.res2 = 0;
    request.pad1 = 0;
    request.pad2 = 0;
    request.sequence = 1;
    request.attempt = htons(1);
    request.version = PROTO_VERSION_NUMBER;
    n = send(sock, &request, sizeof(request), 0);
    when_false_trace(0 <= n, clean_up, ERROR, "Unable to send command to NTP daemon");

    n = wait_for_response(sock);
    when_false_trace(0 < n, clean_up, ERROR, "Select return: %s", (n == -1 ? "error" : "timeout"));

    n = recv(sock, &resp, sizeof(resp), 0);
    when_false_trace(0 < n, clean_up, ERROR, "Unable to talk with NTP daemon");

    state = is_leap_indicator_unsynchronized(resp.data.leap_status) ? Unsynchronized : Synchronized;

clean_up:
    close(sock);

exit:
    return state;
}

bool is_leap_indicator_unsynchronized(uint16_t leap) {
    uint8_t leap_indicator = ntohs(leap);
    return LEAP_INDICATOR_UNSYNC == leap_indicator;
}
