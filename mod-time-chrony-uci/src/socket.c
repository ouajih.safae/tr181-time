/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/time.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>

#include "socket.h"
#include "mod_time_chrony.h"

#define CLEAN_FD(fd) close(fd); fd = -1
#define NTP_DEF_PORT (323u)

static const char* CHRONY_SOCK = "/var/run/chrony/chronyd.sock";
static bool error_reported = false;

static int unix_socket(void);

static bool socket_file_exist(void);

static bool set_nonblock(int socket);

int chrony_def_socket(void) {
    return chrony_socket(NTP_DEF_PORT);
}

int chrony_socket(unsigned int port) {
    int sock = unix_socket();

    if(-1 == sock) {
        sock = ntp_socket(port);
    }

    return sock;
}

static bool socket_file_exist(void) {
    return access(CHRONY_SOCK, F_OK) == 0;
}

static int unix_socket(void) {
    struct sockaddr_un addr;
    int sock = -1;
    bool rc = socket_file_exist();

    when_false_trace(rc, exit, ERROR, "Socket file not exist");

    memset(&addr, 0, sizeof(struct sockaddr_un));

    sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    when_false_trace(-1 != sock, exit, ERROR, "Socket creation error: %d", errno);

    if(false == set_nonblock(sock)) {
        CLEAN_FD(sock);
    } else {
        addr.sun_family = AF_UNIX;
        strncpy(addr.sun_path, CHRONY_SOCK, sizeof(addr.sun_path) - 1);

        if(-1 != connect(sock, (const struct sockaddr*) &addr, sizeof(addr))) {
            if(false == error_reported) {
                SAH_TRACEZ_ERROR(ME, "Cannot connect to chronyd via socket file: Errno =  %d", errno);
                error_reported = true;
            }
            CLEAN_FD(sock);
        } else {
            error_reported = false;
        }
    }

exit:
    return sock;
}

static bool set_nonblock(int socket) {
    int flags = fcntl(socket, F_GETFL, 0);
    bool rc = (-1 != flags);

    when_false_trace(rc, exit, ERROR, "Cannot read FD flags %d", errno);

    if(-1 == fcntl(socket, F_SETFL, flags | O_NONBLOCK)) {
        rc = false;
        SAH_TRACEZ_ERROR(ME, "Unable to set FD flags %d", errno);
    }

exit:
    return rc;
}

int ntp_socket(unsigned int port) {
    int sock = -1;
    struct sockaddr_in in_sock;
    struct in_addr address = {
        .s_addr = inet_addr("127.0.0.1")
    };

    memset(&in_sock, 0, sizeof(struct sockaddr_in));
    in_sock.sin_family = AF_INET;
    in_sock.sin_addr = address;
    in_sock.sin_port = htons(port);

    sock = socket(PF_INET, SOCK_DGRAM, 0);
    when_false_trace(-1 != sock, exit, ERROR, "Socket creation error: %d", errno);

    if(-1 == connect(sock, (const struct sockaddr*) &in_sock, sizeof(in_sock))) {
        SAH_TRACEZ_ERROR(ME, "Cannot connect to NTP daemon on port %d", port);
        close(sock);
        sock = -1;
    }

exit:
    return sock;
}

int wait_for_response(int sock) {
    struct timeval tv = {
        .tv_sec = 5,
        .tv_usec = 0
    };

    fd_set fds;
    memset(&fds, 0, sizeof(fd_set));

    FD_SET(sock, &fds);
    return select(sock + 1, &fds, NULL, NULL, &tv);
}
