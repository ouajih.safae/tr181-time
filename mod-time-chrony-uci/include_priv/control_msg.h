/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__CONTROL_MSG_H__)
#define __CONTROL_MSG_H__
#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#define MAX_PADDING_LENGTH (484)
#define DATA_PADDING (356)

#define CMD_REQUEST (1)
#define CMD_REPLY (2)
#define PROTO_VERSION_NUMBER (6)
#define REQ_TRACKING (33)


typedef struct {
    uint8_t version;
    uint8_t pkt_type;
    uint8_t res1;
    uint8_t res2;
    uint16_t command;
    uint16_t attempt;

    uint32_t sequence;
    uint32_t pad1;
    uint32_t pad2;
    uint8_t data[DATA_PADDING];
    uint8_t padding[MAX_PADDING_LENGTH];
} chrony_trac_req_t;

typedef struct {
    union {
        uint32_t in4;
        uint8_t in6[16];
        uint32_t id;
    } addr;
    uint16_t family;
    uint16_t _pad;
} ip_addr_t;

typedef struct {
    int32_t f;
} float_t;

typedef struct {
    uint32_t tv_sec_high;
    uint32_t tv_sec_low;
    uint32_t tv_nsec;
} timespec_t;

typedef struct {
    uint32_t ref_id;
    ip_addr_t ip_addr;
    uint16_t stratum;
    uint16_t leap_status;
    timespec_t ref_time;
    float_t current_correction;
    float_t last_offset;
    float_t rms_offset;
    float_t freq_ppm;
    float_t resid_freq_ppm;
    float_t skew_ppm;
    float_t root_delay;
    float_t root_dispersion;
    float_t last_update_interval;
    int32_t EOR;
} tracking_data_t;

typedef struct {
    uint8_t version;
    uint8_t pkt_type;
    uint8_t res1;
    uint8_t res2;
    uint16_t command;
    uint16_t reply;
    uint16_t status;
    uint16_t pad1;
    uint16_t pad2;
    uint16_t pad3;
    uint32_t sequence;
    uint32_t pad4;
    uint32_t pad5;
    tracking_data_t data;
} chrony_trac_rep_t;

#ifdef __cplusplus
}
#endif
#endif //__CONTROL_MSG_H__
