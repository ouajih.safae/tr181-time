/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "test_utils.h"
#include "time-manager.h"
#include "client/time-manager_client.h"
#include "utils.h"

#include "test_client.h"

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

void test_client_enable(UNUSED void** state) {
    amxd_object_t* client = amxd_dm_findf(test_get_dm(), "Time.Client.1.");
    amxd_object_t* time = amxd_dm_findf(test_get_dm(), "Time.");
    char* status = NULL;

    test_time_set_enable(true);

    assert_non_null(client);
    status = amxd_object_get_value(cstring_t, client, "Status", NULL);

    assert_non_null(status);
    assert_string_equal("Error", status);
    free(status);

    test_time_set_enable(false);

    status = amxd_object_get_value(cstring_t, client, "Status", NULL);

    assert_non_null(status);
    assert_string_equal("Disabled", status);
    free(status);

    test_time_set_enable(true);

    test_time_client_set_enable(false, 1);
    status = amxd_object_get_value(cstring_t, client, "Status", NULL);
    assert_non_null(status);
    assert_string_equal("Disabled", status);
    free(status);

    status = amxd_object_get_value(cstring_t, time, "Status", NULL);
    assert_non_null(status);
    assert_string_equal("Disabled", status);
    free(status);

    test_time_client_set_enable(true, 1);

    status = amxd_object_get_value(cstring_t, client, "Status", NULL);
    assert_non_null(status);
    assert_string_equal("Error", status);
    free(status);

    status = amxd_object_get_value(cstring_t, time, "Status", NULL);
    assert_non_null(status);
    assert_string_equal("Error", status);
    free(status);

}

void test_client_interface_changed(UNUSED void** state) {
    amxd_dm_t* dm = test_get_dm();
    amxd_object_t* client = amxd_dm_findf(dm, "Time.Client.1");
    amxd_trans_t transaction;
    char* intf_name = NULL;
    amxd_trans_init(&transaction);

    amxd_trans_select_pathf(&transaction, "Time.Client.1.");
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &transaction, "Interface", "TEST_INTF");
    amxd_trans_apply(&transaction, dm);
    test_time_handle_events();
    intf_name = amxd_object_get_value(cstring_t, client, "Interface", NULL);
    assert_string_equal(intf_name, "TEST_INTF");
    free(intf_name);

    amxd_trans_clean(&transaction);
    amxd_trans_select_pathf(&transaction, "Time.Client.1.");
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &transaction, "Interface", "Device.IP.Interface.2.");
    amxd_trans_apply(&transaction, dm);
    test_time_handle_events();
    intf_name = amxd_object_get_value(cstring_t, client, "Interface", NULL);
    assert_string_equal(intf_name, "Device.IP.Interface.2.");
    free(intf_name);
    amxd_trans_clean(&transaction);
}

void test_client_add_delete(UNUSED void** state) {
    amxd_object_t* client_instance = amxd_dm_findf(test_get_dm(), "Time.Client.2");
    amxd_object_t* client_object = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;

    assert_null(client_instance);

    client_object = amxd_dm_findf(test_get_dm(), "Time.Client");
    assert_non_null(client_object);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "Interface", "Device.IP.Interface.2.");

    assert_int_equal(amxd_object_invoke_function(client_object, "_add", &args, &ret), amxd_status_ok);
    assert_false(amxc_var_is_null(&ret));

    handle_events();

    client_instance = amxd_dm_findf(test_get_dm(), "Time.Client.2");
    assert_non_null(client_instance);

    amxd_object_delete(&client_instance);
    handle_events();
    assert_null(client_instance);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

