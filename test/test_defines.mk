MACHINE = $(shell $(CC) -dumpmachine)
SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include ../mocks ../test_utils)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c)

CFG_SOURCES = $(wildcard $(SRCDIR)/configuration/*.c)
CLIENT_SOURCES = $(wildcard $(SRCDIR)/client/*.c)
SERVER_SOURCES = $(wildcard $(SRCDIR)/server/*.c)

CFG_OBJECTS = $(addprefix $(OBJDIR)/configuration/,$(notdir $(CFG_SOURCES:.c=.o)))
CLIENT_OBJECTS = $(addprefix $(OBJDIR)/client/,$(notdir $(CLIENT_SOURCES:.c=.o)))
SERVER_OBJECTS = $(addprefix $(OBJDIR)/server/,$(notdir $(SERVER_SOURCES:.c=.o)))

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		   -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread -DUNIT_TEST \
		   -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lamxb -lamxc -lamxp -lamxd -lamxo -ldl -lsahtrace -lnetmodel -lamxm
