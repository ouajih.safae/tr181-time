/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "time-manager.h"
#include "client/ntp_state.h"
#include "client/ntp_object.h"
#include "../test_utils/test_utils.h"
#include "test_status_parameter.h"

#include "ntp_state_mock.h"
#include "service_mock.h"

static ntp_sync_state_t ntp_sync_status(void);
static void wait_for_timer_event(void);

void test_status_disable_when_enable_par_eq_false(UNUSED void** state) {
    test_time_set_enable(false);
    assert_true(test_time_enable_is(false));
    assert_int_equal(ntp_sync_status(), Disabled);
}

void test_status_poll_interval_change_when_sync(UNUSED void** state) {
    test_time_set_enable(false);
    assert_true(test_time_enable_is(false));
    assert_int_equal(ntp_sync_status(), Disabled);
    assert_int_equal(ntp_sync_state_polling_interval(), SYNC_NOT_REACHED_POLL_INTERVAL);

    mock_set_ntp_status(Unsynchronized);
    assert_int_equal(ntp_sync_status(), Disabled);

    test_time_set_enable(true);
    wait_for_timer_event();
    assert_int_equal(ntp_sync_status(), Unsynchronized);

    mock_set_ntp_status(Synchronized);
    wait_for_timer_event();
    assert_int_equal(ntp_sync_status(), Synchronized);
    assert_int_equal(ntp_sync_state_polling_interval(), SYNC_REACHED_POLL_INTERVAL);

    test_time_set_enable(false);
    assert_int_equal(ntp_sync_status(), Disabled);
    assert_int_equal(ntp_sync_state_polling_interval(), SYNC_NOT_REACHED_POLL_INTERVAL);
}

void test_status_error_when_ntp_daemon_crash(UNUSED void** state) {
    test_time_set_enable(false);
    test_time_handle_events();

    mock_set_ntp_status(Unsynchronized);
    assert_int_equal(ntp_sync_status(), Disabled);

    test_time_set_enable(true);
    wait_for_timer_event();
    assert_int_equal(ntp_sync_status(), Unsynchronized);

    mock_set_ntp_status(Synchronized);
    wait_for_timer_event();
    assert_int_equal(ntp_sync_status(), Synchronized);

    mock_set_service_running(false);
    wait_for_timer_event();
    assert_int_equal(ntp_sync_status(), Error);

}

static ntp_sync_state_t ntp_sync_status(void) {
    amxd_dm_t* dm = test_get_dm();
    amxd_object_t* time_plugin = NULL;
    char* status_str = NULL;
    ntp_sync_state_t status = Error;

    assert_non_null(dm);
    time_plugin = amxd_dm_findf(dm, "Time");
    assert_non_null(time_plugin);

    status_str = amxd_object_get_value(cstring_t, time_plugin, "Status", NULL);
    status = sync_state_from_string(status_str);

    free(status_str);
    return status;
}

static void wait_for_timer_event(void) {
    test_time_read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();
}