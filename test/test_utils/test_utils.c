/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "time-manager.h"
#include "dm_time-manager.h"
#include "client/time-manager_client.h"
#include "client/dm_time-manager_client.h"
#include "server/dm_time-manager_server.h"
#include "test_utils.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxp_signal_t* signal_handler = NULL;

static const char* test_odl_defs = "../test_utils/time-manager_test.odl";
static const char* odl_defs = "../../odl/time-manager_definition.odl";

amxd_dm_t* test_get_dm(void) {
    return &dm;
}

amxo_parser_t* test_get_parser(void) {
    return &parser;
}

int test_time_plugin_teardown(UNUSED void** state) {
    _time_main(1, &dm, &parser);

    amxp_signal_delete(&signal_handler);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

int test_time_plugin_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    assert_int_equal(amxp_signal_new(NULL, &signal_handler, strsignal(SIGCHLD)), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "print_event", AMXO_FUNC(_print_event)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "time_toggle", AMXO_FUNC(_time_toggle)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_client_toggle", AMXO_FUNC(_dm_client_toggle)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_client_added", AMXO_FUNC(_dm_client_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_client_changed", AMXO_FUNC(_dm_client_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "dm_client_interface_changed", AMXO_FUNC(_dm_client_interface_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "update_status", AMXO_FUNC(_update_status)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "update_local_timezone", AMXO_FUNC(_update_local_timezone)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "read_current_local_time", AMXO_FUNC(_read_current_local_time)), 0);
    assert_int_equal(
        amxo_resolver_ftab_add(&parser, "check_local_timezone", AMXO_FUNC(_LocalTimeZone_check_local_timezone)),
        0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "server_added", AMXO_FUNC(_server_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "server_changed", AMXO_FUNC(_server_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "server_interface_changed", AMXO_FUNC(_server_interface_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "count_changed", AMXO_FUNC(_count_changed)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, test_odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    amxc_var_add_key(amxc_htable_t, &parser.config, "time", NULL);

    _time_main(0, &dm, &parser);

    test_time_handle_events();

    return 0;
}

void test_time_handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

bool test_time_enable_is(const bool state) {
    amxd_dm_t* dm = test_get_dm();
    amxd_object_t* time_plugin = NULL;

    assert_non_null(dm);
    time_plugin = amxd_dm_findf(dm, "Time");
    assert_non_null(time_plugin);

    return state == amxd_object_get_value(bool, time_plugin, "Enable", NULL);
}

void test_time_set_enable(bool state) {
    amxd_dm_t* dm = test_get_dm();
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);

    amxd_trans_select_pathf(&transaction, "Time.");
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_value(bool, &transaction, "Enable", state);
    amxd_trans_apply(&transaction, dm);
    test_time_handle_events();

    amxd_trans_clean(&transaction);
}

void test_time_server_set_enable(bool state, uint32_t index) {
    amxd_dm_t* dm = test_get_dm();
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);

    amxd_trans_select_pathf(&transaction, "Time.Server.%d.", index);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_value(bool, &transaction, "Enable", state);
    amxd_trans_apply(&transaction, dm);
    test_time_handle_events();

    amxd_trans_clean(&transaction);
}

void test_time_client_set_enable(bool state, uint32_t index) {
    amxd_dm_t* dm = test_get_dm();
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);

    amxd_trans_select_pathf(&transaction, "Time.Client.%d.", index);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_value(bool, &transaction, "Enable", state);
    amxd_trans_apply(&transaction, dm);
    test_time_handle_events();

    amxd_trans_clean(&transaction);
}

void test_time_read_sigalrm(void) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        printf("Got SIGALRM\n");
    } else {
        printf("Read unexpected signal\n");
    }
}

