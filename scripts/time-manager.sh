#!/bin/sh
[ -f /etc/environment ] && source /etc/environment
ulimit -c ${ULIMIT_CONFIGURATION:-0}
name="time-manager"

case $1 in
    start|boot)
        time-manager -D
        ;;
    stop|shutdown)
        if [ -f /var/run/${name}.pid ]; then
            kill `cat /var/run/${name}.pid`
        else
            killall ${name}
        fi
        ;;
    debuginfo)
        ubus-cli "Time.?"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    log)
        echo "TODO log tr181 time plugin plugin"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac
