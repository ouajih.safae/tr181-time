/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <stdio.h>

#include "time-manager.h"
#include "client/ntp_object.h"
#include "client/time-manager_client.h"
#include "configuration/time_configuration.h"
#include "server/netmodel.h"
#include "server/time-manager_server.h"

static time_app_t* app = NULL;

static void toggle_ntp_daemon_state(amxd_object_t* time_object) {
    when_null(time_object, failed);
    uint32_t interval = ntp_sync_state_polling_interval();
    bool value = amxd_object_get_value(bool, time_object, "Enable", NULL);

    if(true == value) {
        amxp_timer_set_interval(app->timer, interval);
        if(amxp_timer_get_state(app->timer) != amxp_timer_running) {
            amxp_timer_start(app->timer, interval);
        }
    } else {
failed:
        (void) amxp_timer_stop(app->timer);
    }
}

static void check_ntp_sync_status_cb(UNUSED amxp_timer_t* const timer, void* data) {
    amxd_object_t* time_object = (amxd_object_t*) data;
    update_time_and_clients_status(time_object);
}

void time_init(time_app_t* time_app) {
    if(NULL != time_app) {
        amxd_object_t* instance = amxd_dm_findf(time_get_dm(), "Time.");
        app = time_app;
        amxp_timer_new(&app->timer, check_ntp_sync_status_cb, instance);
        ntp_sync_state_register_timer(app->timer);
        if(NULL != instance) {
            update_time_config_enable(instance);
            time_config_update(instance);
        }
    }
}

void time_cleanup(void) {
    if(NULL != app) {
        ntp_sync_state_register_timer(NULL);
        amxp_timer_delete(&app->timer);
        app = NULL;
    }
}

void update_time_config_enable(amxd_object_t* time_object) {
    toggle_ntp_daemon_state(time_object);
}

amxd_status_t update_time_dm_status_parameter(const char* value) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxd_trans_t transaction;
    amxd_object_t* time = amxd_dm_get_object(time_get_dm(), "Time");
    when_null(time, exit);
    when_str_empty(value, exit);

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&transaction, time);
    amxd_trans_set_value(cstring_t, &transaction, "Status", value);

    rc = amxd_trans_apply(&transaction, time_get_dm());
    amxd_trans_clean(&transaction);

exit:
    return rc;
}

bool is_initialized(amxd_object_t* time_object) {
    bool initialized = false;
    amxd_object_t* clients = NULL;
    when_null(time_object, exit);
    clients = amxd_object_get_child(time_object, "Client");
    when_null(clients, exit);

    amxd_object_for_each(instance, iter, clients) {
        amxd_object_t* client = amxc_llist_it_get_data(iter, amxd_object_t, it);
        when_null(client, next);
        client_info_t* info = (client_info_t*) client->priv;
        when_null(info, next);
        if(info->initial_sync) {
            initialized = true;
        }
next:
        continue;
    }

exit:
    return initialized;
}

void set_initialized(amxd_object_t* client) {
    when_null(client, exit);
    amxd_object_t* servers = NULL;
    amxd_object_t* time = NULL;
    client_info_t* info = (client_info_t*) client->priv;
    when_null(info, exit);
    info->initial_sync = true;

    time = amxd_dm_get_object(time_get_dm(), "Time");
    when_null(time, exit);
    servers = amxd_object_get_child(time, "Server");
    when_null(servers, exit);

    amxd_object_for_each(instance, iter, servers) {
        amxd_object_t* server = amxc_llist_it_get_data(iter, amxd_object_t, it);
        when_null(server, next);
        time_server_refresh(server);
next:
        continue;
    }
    time_config_update(time);
exit:
    return;
}
