/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include "time-manager.h"
#include "client/ntp_object.h"
#include "configuration/time_configuration.h"
#include "ntp_daemon/service.h"
#include "server/time-manager_server.h"
#include "server/firewall.h"
#include "server/netmodel.h"
#include "utils.h"
#include "netmodel/common_api.h"

static const char* time_server_status_string[] = {
    "Disabled",
    "Enabled",
    "Error"
};

static const char* time_server_status_to_string(time_server_status_t status) {
    return TimeServer_Disabled <= status && TimeServer_Error >= status ? time_server_status_string[status] : "Unsupported";
}

static time_server_status_t time_server_status_from_string(const char* status_str) {
    time_server_status_t status = TimeServer_Disabled;
    if(NULL != status_str) {
        for(int i = Disabled; i <= Error; ++i) {
            if(strcmp(status_str, time_server_status_string[i]) == 0) {
                status = (time_server_status_t) i;
                break;
            }
        }
    }
    return status;
}

static time_server_status_t time_server_current_status(amxd_object_t* server) {
    time_server_status_t status = TimeServer_Error;
    char* status_str = NULL;
    status_str = amxd_object_get_value(cstring_t, server, "Status", NULL);
    when_null(status_str, exit);
    status = time_server_status_from_string(status_str);

exit:
    free(status_str);
    return status;
}


static amxd_status_t update_dm_time_server_status_parameter(amxd_object_t* server_instance, time_server_status_t status) {
    amxd_trans_t transaction;
    amxd_status_t rc = amxd_status_unknown_error;

    when_null(server_instance, exit);

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&transaction, server_instance);
    amxd_trans_set_value(cstring_t, &transaction, "Status", time_server_status_to_string(status));

    rc = amxd_trans_apply(&transaction, time_get_dm());
    amxd_trans_clean(&transaction);

exit:
    return rc;
}


void time_server_cleanup(void) {
    firewall_close_ntp_port();
}

amxd_status_t time_server_refresh(amxd_object_t* server) {
    amxd_status_t ret = amxd_status_parameter_not_found;
    const time_server_status_t current_status = time_server_current_status(server);
    time_server_status_t status = TimeServer_Error;
    server_info_t* info = NULL;
    amxd_object_t* time_object = NULL;
    amxd_object_t* servers_object = NULL;
    bool is_enabled = false;
    bool time_enabled = false;
    bool time_initialized = false;

    when_null(server, exit);
    info = (server_info_t*) server->priv;

    time_object = amxd_dm_findf(time_get_dm(), "Time.");
    when_null(time_object, exit);
    time_enabled = time_dm_get_enable(time_object);
    time_initialized = is_initialized(time_object);

    is_enabled = time_dm_get_enable(server);

    if((false == time_enabled) || (false == is_enabled)) {
        status = TimeServer_Disabled;
    } else {
        if(NULL == info) {
            status = TimeServer_Error;
        } else {
            status = (get_state_server(info) ? TimeServer_Enabled : TimeServer_Error);
        }
    }

    if(!time_initialized && is_enabled) {
        status = TimeServer_Error;
    }

    if(current_status != status) {
        firewall_close_ntp_port();
        servers_object = amxd_dm_findf(time_get_dm(), "Time.Server.");
        when_null(servers_object, exit);
        amxd_object_for_all(servers_object, "[Enable == true].", firewall_open_ntp_port, NULL);
        ret = update_dm_time_server_status_parameter(server, status);
        when_failed_trace(ret, exit, ERROR, "Failed to update server status");
    }

    ret = amxd_status_ok;

exit:
    return ret;
}
