/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include "utils.h"
#include "time-manager.h"
#include "client/time-manager_client.h"
#include "server/time-manager_server.h"
#include "server/netmodel.h"
#include "configuration/time_configuration.h"

static void subscribe_all_netmodel(void) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* server_object = NULL;
    amxd_object_t* client_object = NULL;
    amxd_object_t* server = NULL;
    amxd_object_t* client = NULL;

    server_object = amxd_dm_findf(time_get_dm(), "Time.Server.");
    when_null_trace(server_object, exit, WARNING, "No Server object found");
    client_object = amxd_dm_findf(time_get_dm(), "Time.Client");
    when_null_trace(client_object, exit, WARNING, "No Client object found");

    amxd_object_for_each(instance, it, server_object) {
        SAH_TRACEZ_INFO(ME, "Creating server info");
        server = amxc_container_of(it, amxd_object_t, it);
        create_server_info(server);
    }
    amxd_object_for_each(instance, it, client_object) {
        SAH_TRACEZ_INFO(ME, "Creating client info");
        client = amxc_container_of(it, amxd_object_t, it);
        create_client_info(client);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void unsubscribe_all_netmodel(void) {
    amxd_object_t* server_object = NULL;
    amxd_object_t* client_object = NULL;

    server_object = amxd_dm_findf(time_get_dm(), "Time.Server.");
    when_null(server_object, exit);
    client_object = amxd_dm_findf(time_get_dm(), "Time.Client");
    when_null(client_object, exit);

    amxd_object_for_each(instance, it, server_object) {
        amxd_object_t* server = amxc_container_of(it, amxd_object_t, it);
        clear_server_info((server_info_t*) server->priv);
    }
    amxd_object_for_each(instance, it, client_object) {
        amxd_object_t* client = amxc_container_of(it, amxd_object_t, it);
        clear_client_info((client_info_t*) client->priv);
    }
exit:
    return;
}

static void server_changed_state_cb(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    amxd_status_t status = amxd_status_unknown_error;
    server_info_t* info = (server_info_t*) priv;
    amxd_object_t* time_object = NULL;

    when_null(info, exit);
    when_null(info->obj, exit);
    when_null(data, exit);

    // Check if query is opened correctly
    if(amxc_var_is_null(data)) {
        if(strcmp(sig_name, "Resolver") == 0) {
            SAH_TRACEZ_INFO(ME, "Query interface path is still being resolved");
        } else {
            SAH_TRACEZ_ERROR(ME, "Query is closed in NetModel, closing local query");
            netmodel_closeQuery(info->query);
            info->query = NULL;
        }
        goto exit;
    }

    info->state = amxc_var_dyncast(bool, data);

    status = time_server_refresh(info->obj);
    time_object = amxd_dm_findf(time_get_dm(), "Time.");
    when_null(time_object, exit);
    time_config_update(time_object);

    when_failed(status, exit);

    SAH_TRACEZ_INFO(ME, "Successfully changed server object %s state to %d", info->obj->name, info->state);
    when_failed_trace(status, exit, ERROR, "Failed to update the state for object");
exit:
    return;
}

static void client_changed_state_cb(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    client_info_t* info = (client_info_t*) priv;
    amxd_object_t* time_object = time_dm_get_object("Time");

    when_null(info, exit);
    when_null(info->obj, exit);

    // Check if query is opened correctly
    if(amxc_var_is_null(data)) {
        if(strcmp(sig_name, "Resolver") == 0) {
            SAH_TRACEZ_INFO(ME, "Query interface path is still being resolved");
        } else {
            SAH_TRACEZ_ERROR(ME, "Query is closed in NetModel, closing local query");
            netmodel_closeQuery(info->query);
            info->query = NULL;
        }
        goto exit;
    }

    info->state = amxc_var_dyncast(bool, data);

    when_null(time_object, exit);
    time_config_update(time_object);
    update_time_and_clients_status(time_object);
    SAH_TRACEZ_INFO(ME, "Client interface is %s",
                    info->state ? "up" : "not up, time will not be able to synchronize");

exit:
    return;
}

static char* to_intf(const char* address, uint32_t prefix) {
    amxc_string_t buf;
    char* network = NULL;

    amxc_string_init(&buf, 0);
    when_str_empty(address, exit);

    amxc_string_setf(&buf, "%s/%d", address, prefix);
    network = amxc_string_take_buffer(&buf);

exit:
    amxc_string_clean(&buf);
    return network;
}
static void server_ip_cb(UNUSED const char* sig_name,
                         const amxc_var_t* data,
                         UNUSED void* priv) {
    server_info_t* info = (server_info_t*) priv;
    amxc_var_t* var = NULL;
    const char* intf_addr = NULL;
    char* interface = NULL;
    uint32_t intf_prefix;
    amxd_object_t* time_object = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    var = GETI_ARG(data, 0);

    // Check if query is opened correctly
    if(amxc_var_is_null(data)) {
        if(strcmp(sig_name, "Resolver") == 0) {
            SAH_TRACEZ_INFO(ME, "Query interface path is still being resolved");
        } else {
            SAH_TRACEZ_ERROR(ME, "Query is closed in NetModel, closing local query");
            netmodel_closeQuery(info->query);
            info->query = NULL;
        }
        goto exit;
    }

    intf_addr = GETP_CHAR(var, "Address");
    intf_prefix = GETP_UINT32(var, "PrefixLen");

    free(info->intf_interface);
    info->intf_interface = NULL;

    interface = to_intf(intf_addr, intf_prefix);

    info->intf_interface = interface;
    interface = NULL;

    status = time_server_refresh(info->obj);
    time_object = amxd_dm_findf(time_get_dm(), "Time.");
    when_null(time_object, exit);
    time_config_update(time_object);
    when_failed_trace(status, exit, ERROR, "Failed to update the interface object");
exit:
    free(interface);
    return;
}

static void open_server_nm_queries(cstring_t ip_path, server_info_t* info) {
    info->query = netmodel_openQuery_isUp(ip_path, "time-manager", NULL, netmodel_traverse_this,
                                          server_changed_state_cb, (void*) info);
    info->addr_query = netmodel_openQuery_getAddrs(ip_path, "time-manager", "ipv4", netmodel_traverse_down,
                                                   server_ip_cb, (void*) info);
}

static void open_client_nm_queries(cstring_t ip_path, client_info_t* info) {
    info->query = netmodel_openQuery_isUp(ip_path, "time-manager", NULL, netmodel_traverse_down,
                                          client_changed_state_cb, (void*) info);
}

static void close_server_nm_queries(server_info_t* info) {
    if(info->query != NULL) {
        netmodel_closeQuery(info->query);
        info->query = NULL;
    }
    if(info->addr_query != NULL) {
        netmodel_closeQuery(info->addr_query);
        info->addr_query = NULL;
    }
}

static void close_client_nm_queries(client_info_t* info) {
    if(info->query != NULL) {
        netmodel_closeQuery(info->query);
        info->query = NULL;
    }
}

amxd_status_t create_client_info(amxd_object_t* obj) {
    client_info_t* info = NULL;
    cstring_t ip_path = NULL;
    amxd_status_t status = amxd_status_object_not_found;

    when_null_trace(obj, exit, ERROR, "Failed to subscribe on netmodel, object was not found");
    when_false_trace(obj->priv == NULL, exit, ERROR, "Failed to subscribe on netmodel, priv was not null. For object: %s", obj->name);

    info = (client_info_t*) calloc(1, sizeof(client_info_t));
    obj->priv = info;
    info->obj = obj;
    info->state = false;
    info->initial_sync = false;

    ip_path = amxd_object_get_value(cstring_t, obj, "Interface", NULL);
    open_client_nm_queries(ip_path, info);
    free(ip_path);

    if(info->query != NULL) {
        status = amxd_status_ok;
        SAH_TRACEZ_INFO(ME, "Successfully added a query from netmodel for client %s", obj->name);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to add a query from netmodel");
        clear_client_info((client_info_t*) obj->priv);
        status = amxd_status_unknown_error;
    }

exit:
    return status;
}

amxd_status_t update_client_info(amxd_object_t* obj) {
    client_info_t* info = NULL;
    amxd_status_t status = amxd_status_object_not_found;
    cstring_t ip_path = NULL;

    when_null(obj, exit);

    if(obj->priv == NULL) {
        status = create_client_info(obj);
    } else {
        info = (client_info_t*) obj->priv;
        close_client_nm_queries(info);
        info->state = false;

        ip_path = amxd_object_get_value(cstring_t, obj, "Interface", NULL);
        SAH_TRACEZ_INFO(ME, "Open client queries for Interface \"%s\", object name = \"%s\"", ip_path, obj->name);
        open_client_nm_queries(ip_path, info);
        free(ip_path);

        if(info->query != NULL) {
            status = amxd_status_ok;
            SAH_TRACEZ_INFO(ME, "Successfully updated a query from netmodel for intf %s", obj->name);
        } else {
            SAH_TRACEZ_ERROR(ME, "Failed to update a query form netmodel");
            clear_client_info((client_info_t*) info);
            status = amxd_status_unknown_error;
        }
    }
exit:
    return status;
}

amxd_status_t create_server_info(amxd_object_t* obj) {
    server_info_t* info = NULL;
    cstring_t ip_path = NULL;
    amxd_status_t status = amxd_status_object_not_found;
    amxd_object_t* time_object = NULL;

    when_null_trace(obj, exit, ERROR, "Failed to subscribe on netmodel, object was not found");
    when_false_trace(obj->priv == NULL, exit, ERROR, "Failed to subscribe on netmodel, priv was not null. For object: %s", obj->name);

    info = (server_info_t*) calloc(1, sizeof(server_info_t));
    obj->priv = info;
    info->obj = obj;

    ip_path = amxd_object_get_value(cstring_t, obj, "Interface", NULL);
    SAH_TRACEZ_INFO(ME, "Open server queries for Interface \"%s\", object name = \"%s\"", ip_path, obj->name);
    open_server_nm_queries(ip_path, info);
    free(ip_path);
    if((info->query != NULL) && (info->addr_query != NULL)) {
        status = amxd_status_ok;
        SAH_TRACEZ_INFO(ME, "Successfully added a query from netmodel for intf %s", obj->name);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to add a query from netmodel");
        clear_server_info((server_info_t*) obj->priv);
        obj->priv = NULL;
        status = amxd_status_unknown_error;

        time_server_refresh(obj);
        time_object = amxd_dm_findf(time_get_dm(), "Time.");
        when_null(time_object, exit);
        time_config_update(time_object);
    }
exit:
    return status;
}

amxd_status_t update_server_info(amxd_object_t* obj) {
    server_info_t* info = NULL;
    amxd_status_t status = amxd_status_object_not_found;
    cstring_t ip_path = NULL;
    amxd_object_t* time_object = NULL;

    when_null(obj, exit);

    if(obj->priv == NULL) {
        SAH_TRACEZ_INFO(ME, "Going to create server info");
        status = create_server_info(obj);
    } else {
        info = (server_info_t*) obj->priv;
        close_server_nm_queries(info);

        ip_path = amxd_object_get_value(cstring_t, obj, "Interface", NULL);
        SAH_TRACEZ_INFO(ME, "Open server queries for Interface \"%s\", object name = \"%s\"", ip_path, obj->name);
        open_server_nm_queries(ip_path, info);
        free(ip_path);
        if((info->query != NULL) && (info->addr_query != NULL)) {
            status = amxd_status_ok;
            SAH_TRACEZ_INFO(ME, "Successfully updated a query from netmodel for intf %s", obj->name);
        } else {
            SAH_TRACEZ_ERROR(ME, "Failed to update a query from netmodel");
            clear_server_info((server_info_t*) info);
            status = amxd_status_unknown_error;
            when_failed(time_server_refresh(obj), exit);
            time_object = amxd_dm_findf(time_get_dm(), "Time.");
            when_null(time_object, exit);
            time_config_update(time_object);
        }
    }
exit:
    return status;
}


void clear_client_info(client_info_t* info) {
    when_null(info, exit);

    close_client_nm_queries(info);
    if(info->obj != NULL) {
        info->obj->priv = NULL;
        info->obj = NULL;
    }

    free(info);

exit:
    return;
}

void clear_server_info(server_info_t* info) {
    when_null(info, exit);

    close_server_nm_queries(info);
    free(info->intf_interface);
    info->intf_interface = NULL;

    if(info->obj != NULL) {
        info->obj->priv = NULL;
        info->obj = NULL;
    }
    free(info);
exit:
    return;
}

bool get_state_client(client_info_t* info) {
    bool ret = false;
    when_null(info, exit);
    ret = info->state;
exit:
    return ret;
}

bool get_state_server(server_info_t* info) {
    bool ret = false;
    when_null(info, exit);
    ret = info->state;
exit:
    return ret;
}

void time_netmodel_init(void) {
    netmodel_initialize();
    subscribe_all_netmodel();
}

void time_netmodel_cleanup(void) {
    unsubscribe_all_netmodel();// unsubscribe needed because _del_interface is only called after netmodel_cleanup
    netmodel_cleanup();
}
