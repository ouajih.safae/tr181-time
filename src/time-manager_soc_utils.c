/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "time-manager.h"
#include "time-manager_soc_utils.h"
#include "server/netmodel.h"
#include <debug/sahtrace.h>
#include <amxc/amxc_variant.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>

static void mod_time_build_time_structure(amxd_object_t* time, amxc_var_t* data) {
    const char* timezone = NULL;
    uint32_t clients = 0;
    uint32_t servers = 0;
    bool enable = false;
    when_null(time, exit);
    timezone = amxc_var_constcast(cstring_t, amxd_object_get_param_value(time, "LocalTimeZone"));
    clients = amxd_object_get_value(uint32_t, time, "ClientNumberOfEntries", NULL);
    servers = amxd_object_get_value(uint32_t, time, "ServerNumberOfEntries", NULL);
    enable = time_dm_get_enable(time);

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, data, "enable", enable);
    amxc_var_add_key(cstring_t, data, "timezone", timezone);
    amxc_var_add_key(uint32_t, data, "clients", clients);
    amxc_var_add_key(uint32_t, data, "servers", servers);

exit:
    return;
}

static void mod_time_append_client(amxd_object_t* client, amxc_var_t* data, uint32_t index) {
    bool enable = false;
    amxc_var_t* client_data = NULL;
    amxc_var_t* lservers = NULL;
    uint32_t poll;
    uint32_t maxpoll;
    uint32_t minpoll;
    bool iburst;
    const amxc_var_t* servers = NULL;
    amxc_string_t client_name;

    enable = time_dm_get_enable(client);
    servers = amxd_object_get_param_value(client, "Servers");

    poll = amxd_object_get_value(uint32_t, client, "Poll", NULL);
    maxpoll = amxd_object_get_value(uint32_t, client, "MaxPoll", NULL);
    minpoll = amxd_object_get_value(uint32_t, client, "MinPoll", NULL);
    iburst = amxd_object_get_value(bool, client, "IBurst", NULL);

    amxc_string_init(&client_name, 0);
    when_failed(amxc_string_setf(&client_name, "%s%d", "client", index), exit);

    client_data = amxc_var_add_key(amxc_htable_t, data, amxc_string_get(&client_name, 0), NULL);
    amxc_var_add_key(bool, client_data, "enable", enable);
    lservers = amxc_var_add_key(amxc_llist_t, client_data, "servers", NULL);
    amxc_var_convert(lservers, servers, AMXC_VAR_ID_LIST);
    amxc_var_add_key(uint32_t, client_data, "poll", poll);
    amxc_var_add_key(uint32_t, client_data, "maxpoll", maxpoll);
    amxc_var_add_key(uint32_t, client_data, "minpoll", minpoll);
    amxc_var_add_key(bool, client_data, "iburst", iburst);

exit:
    amxc_string_clean(&client_name);
}

static void mod_time_append_server(amxd_object_t* server, amxc_var_t* data, uint32_t index) {
    bool enable = false;
    server_info_t* info = NULL;
    amxc_var_t* server_data = NULL;
    amxc_string_t server_name;

    enable = time_dm_get_enable(server);

    info = (server_info_t*) server->priv;

    amxc_string_init(&server_name, 0);
    when_failed(amxc_string_setf(&server_name, "%s%d", "server", index), exit);

    server_data = amxc_var_add_key(amxc_htable_t, data, amxc_string_get(&server_name, 0), NULL);
    amxc_var_add_key(bool, server_data, "enable", enable);
    amxc_var_add_key(cstring_t, server_data, "interface", info->intf_interface);

exit:
    amxc_string_clean(&server_name);
}

static void mod_time_build_clients_structure(amxd_object_t* time, amxc_var_t* data) {
    uint32_t index = 0;
    amxd_object_t* clients = NULL;

    when_null(time, exit);
    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    clients = amxd_object_get_child(time, "Client");
    when_null(clients, exit);

    amxd_object_for_each(instance, iter, clients) {
        amxd_object_t* client = amxc_llist_it_get_data(iter, amxd_object_t, it);
        when_null(client, next);
        client_info_t* info = (client_info_t*) client->priv;
        when_null(info, next);
        if(info->state) {
            index += 1;
            mod_time_append_client(client, data, index);
        }
next:
        continue;
    }

exit:
    return;
}

static void mod_time_build_servers_structure(amxd_object_t* time, amxc_var_t* data) {
    uint32_t index = 0;
    bool time_initialized = false;
    amxd_object_t* servers = NULL;

    when_null(time, exit);
    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    time_initialized = is_initialized(time);
    when_false(time_initialized, exit);
    servers = amxd_object_get_child(time, "Server");
    when_null(servers, exit);

    amxd_object_for_each(instance, iter, servers) {
        amxd_object_t* server = amxc_llist_it_get_data(iter, amxd_object_t, it);
        when_null(server, next);
        server_info_t* info = (server_info_t*) server->priv;
        when_null(info, next);
        if(info->state) {
            index += 1;
            mod_time_append_server(server, data, index);
        }
next:
        continue;
    }

exit:
    return;
}

void time_create_variant(amxd_object_t* time, amxc_var_t* data) {
    amxc_var_t* tmp = NULL;
    when_null(time, exit);
    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    tmp = amxc_var_add_key(amxc_htable_t, data, "Time", NULL);
    mod_time_build_time_structure(time, tmp);
    tmp = amxc_var_add_key(amxc_htable_t, data, "Clients", NULL);
    mod_time_build_clients_structure(time, tmp);
    tmp = amxc_var_add_key(amxc_htable_t, data, "Servers", NULL);
    mod_time_build_servers_structure(time, tmp);

exit:
    return;
}
int mod_time_execute_function(const char* function_name, const char* module, amxc_var_t* data) {
    amxc_var_t ret;
    amxc_var_init(&ret);
    int retval = mod_time_execute(function_name, module, data, &ret);

    amxc_var_clean(&ret);
    return retval;
}


int mod_time_execute(const char* function_name, const char* module, amxc_var_t* data, amxc_var_t* ret) {
    int retval = -1;
    when_str_empty(module, exit);
    retval = amxm_execute_function(module, MOD_TIME_CTRL, function_name, data, ret);

exit:
    return retval;
}
